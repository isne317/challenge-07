#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];
		for(int i=0; i<strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)]='\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for(int i=0; i<strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)]='\0';
	}

	StringVar::~StringVar()
	{
		delete [] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}
	
	/*	
		- This only read the first word of the input
	*/
	istream& operator >> (istream& ins, const StringVar& the_string)
	{
		ins >> the_string.value;
	}
	
	/*
		- Return string of the StringVar
	*/
	string StringVar::toString() 
	{
		string output;
		for (int i = 0; i < max_length; i++) 
		{
			output = output + value[i];
		}

		return output;
	}
	
	/*
		- Return the number of lenght of the StringVar
	*/
	int StringVar::maxLength()
	{
		return max_length;
	}
	
	/*
		- Add two string of StringVar together
	*/
	string operator + (StringVar s1, StringVar s2)
	{
		string output;
		output = s1.toString() + s2.toString();
		return output;
	}
	
	/*
		- Return if two StringVar is equal
	*/
	bool operator == (StringVar s1, StringVar s2)
	{
		if((s1.toString() == s2.toString()) && (s1.maxLength() == s2.maxLength())) 
		{
			return true;
		}
		
		return false;
	}
	
	/*
		- Return substring start at index <i> for <j> character(s)
	*/
	string StringVar::copy_piece(int i, int j) 
	{
		string output;
		for (int k = i; k < i + j; k++) 
		{
			output = output + value[k];
		}

		return output;
	}
	
	/*
		- Return a character at index <index> of StringVar
	*/
	char StringVar::one_char(int index)
	{
		return value[index];
	}

	/*
		- Change one character at index <index> of StringVar
	*/
	void StringVar::set_char(int index, char c)
	{
		value[index] = c;
	}
}//strvarken
