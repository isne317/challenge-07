#include <iostream>
#include "strvar.h"
#include "strvar.cpp"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
	using namespace std;
	conversation(30);
	//cout << "End of Demo.\n"; <- It is not a demo anymore!
	return 0;
}

//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), our_name("Borg");

	cout << "What is your name?\n";
	cin >> your_name;
	//your_name.input_line(cin);
	cout << "We are " << our_name << endl;
	cout << "Our first three characters is " << our_name.copy_piece(0, 3) << endl;
	cout << "The first character of your name is '" << your_name.one_char(0) << "', amazing!" << endl;
	if(your_name == our_name)
	{
		cout << "We both shared the same name! What a coincident!" << endl;	
	}
	else
	{
		cout << "We have a different name." << endl;
	}
	our_name.set_char(0, 'G');
	cout << "So, we change our name to " << our_name << endl;
	cout << "We will meet again " << your_name + our_name << endl;
}
